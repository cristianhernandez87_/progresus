/**
 * Styles
 */
import "../scss/index.scss";

/**
 * Modules
 */
import { silcCoreInit } from 'silc-core';


// components
import { menu } from '../components/header/header';


/**
 * Init
 */

silcCoreInit();
menu();